@echo off
REM # @Author: Tárbes Carvalho
REM # @Date: 10-25-2017
REM # Script to apply the "vaccine" against the threat ransomware BadRabbit
REM # The script can be enabled through GPO (Startup Script) for enterprise environments - https://technet.microsoft.com/en-us/library/cc770556(v=ws.11).aspx
REM # More technical information about the threat can be found at the following link: https://www.kaspersky.com/blog/bad-rabbit-ransomware/19887/
REM # Credits to Amit Serper (@0xAmit) and Mike locovacci (@mikeiacovacci)

REM # The function of the script is to create the following blank files: c: \ windows \ infpub.dat and c: \ Windows \ cscc.dat, removing all permissions from those files, thus preventing them from running.

REM # 1. Creating Files Command

echo "" > %windir%\infpub.dat
echo "" > %windir%\cscc.dat

REM # 2. Command to remove permissions from created files

icacls %windir%\infpub.dat /inheritance:r
icacls %windir%\cscc.dat /inheritance:r